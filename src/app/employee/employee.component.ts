import { Component, OnInit, ViewChild } from '@angular/core';
import { EmployeeDetails } from '../employee.model';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  empDetailsList:Array<EmployeeDetails>=[ 
    {"id": 11, "name": "Ash", "department": "Finance", "joining_date":this.formateDate("8/10/2016") },
    {"id": 12,"name": "John","department": "HR","joining_date": this.formateDate("18/1/2011")},
    { "id": 13, "name": "Zuri", "department": "Operations", "joining_date": this.formateDate("28/11/2019")},
    {"id": 14,  "name": "Vish",  "department": "Development",   "joining_date": this.formateDate("7/7/2017")},
    { "id": 15, "name": "Barry",  "department": "Operations", "joining_date": this.formateDate("19/8/2014")},
    {"id": 16,"name": "Ady",  "department": "Finance",  "joining_date": this.formateDate("5/10/2014")}, 
    { "id": 17,"name": "Gare","department": "Development",  "joining_date": this.formateDate("6/4/2014")},
    { "id": 18,  "name": "Hola",  "department": "Development",  "joining_date": this.formateDate("8/12/2010")}, 
    {"id": 19,  "name": "Ola",  "department": "HR",  "joining_date": this.formateDate("7/5/2011")},
    { "id": 20,  "name": "Kim",  "department": "Finance",  "joining_date": this.formateDate("20/10/2010")}
  ];
  empListMoreTwoYear:Array<EmployeeDetails>=[];
  empFilterDevelopmentDepartment :Array<EmployeeDetails>=[];
  formateDate(date:String){
    let tempArray = date.split("/");
    let temp = tempArray[0];
    tempArray[0] = tempArray[1];
    tempArray[1] = temp;

    let stringDate = tempArray.join();
    return new Date(stringDate.replace(",", "/"));
  }
  displayedColumns: string[] = ['id', 'name', 'department', 'joining_date'];
  dataSource: MatTableDataSource<EmployeeDetails> | any;

  @ViewChild(MatPaginator) paginator: MatPaginator | any;
  @ViewChild(MatSort) sort: MatSort | any;
  
  constructor() { }

  ngOnInit(): void {

   

    this.dataSource = new MatTableDataSource(this.empDetailsList);
  }


  getListMoreThanTwoYear(){
    this.empListMoreTwoYear =[];
    this.empDetailsList.forEach(item => {
      
      let yearGap = Number(new Date().getFullYear()) - Number(item.joining_date?.getFullYear());
      if(yearGap>2){
        this.empListMoreTwoYear.push(item);
      }
    });

    this.dataSource = new MatTableDataSource(this.empListMoreTwoYear);
    this.ngAfterViewInit();
  }

  
  deptNameArray: Array<any>=[];

  empDeptDistinctCount(){
    this.deptNameArray=[];
    this.empDetailsList.forEach(item => {
      this.deptNameArray.push(item.department);
    })
    
      this.deptNameArray.sort();
      console.log(this.deptNameArray);
      var count = 1;
      var results = "";
      for (var i = 0; i < this.deptNameArray.length; i++)
      {
          if (this.deptNameArray[i] == this.deptNameArray[i+1])
          {
            count +=1;
          }
          else
          {
              results += this.deptNameArray[i] + " --> " + count + " Candidate\n" ;
              count=1;
          }
      }
      alert(results)
  }

  deleteDevDept(){
    this.empFilterDevelopmentDepartment=[];
    this.empDetailsList.forEach(item => {
      if(item.department!="Development"){
        this.empFilterDevelopmentDepartment.push(item);
      }
    })
    this.dataSource = new MatTableDataSource(this.empFilterDevelopmentDepartment);
    this.ngAfterViewInit();
  }

  getList(){
    this.dataSource = new MatTableDataSource(this.empDetailsList);
    this.ngAfterViewInit();
  }


  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;

  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

}
